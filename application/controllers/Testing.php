<?php

class Testing extends PHPIEM_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	function Index()
	{
		$this->load->model('attendeemodel');
		$this->load->model('appsmodel');
		$res = $this->appsmodel->get_option('admin_event_menu');
		
		$test = '[{"label":"Home","icon":"home","child":[{"label":"Dashboard","url":"\/backpanel"}]},{"label":"Events","icon":"table","child":[{"label":"All Events","url":"\/backpanel\/events"},{"label":"Create Event","url":"\/backpanel\/event\/create"},{"label":"Type","url":"\/backpanel\/event\/event-type"}]},{"label":"Pages","icon":"clone","child":[{"label":"All Pages","url":"\/backpanel\/page"},{"label":"Create Page","url":"\/backpanel\/page\/create"}]},{"label":"Users","icon":"user","child":[{"label":"All Users","url":"\/backpanel\/user"},{"label":"Create User","url":"\/backpanel\/user\/create"}]},{"label":"Site Settings","icon":"gear","child":[{"label":"General","url":"\/backpanel\/site-settings\/general"},{"label":"Email","url":"\/backpanel\/site-settings\/email"}]}]';
		
		$tevent = '[{"label":"Event Home","icon":"home","child":[{"label":"Event Dashboard","url":"\/backpanel\/event\/[eventid]"},{"label":"Switch Event","url":"\/backpanel\/events"}]},{"label":"Registration","icon":"user","child":[{"label":"Check-In","url":"\/backpanel\/event\/[eventid]\/checkin"}]},{"label":"Attendee","icon":"users","child":[{"label":"All Attendee","url":"\/backpanel\/event\/[eventid]\/attendee"},{"label":"Add Attendee","url":"\/backpanel\/event\/[eventid]\/new"}]},{"label":"Feedbacks","icon":"feed","child":[{"label":"All Feedbacks","url":"\/backpanel\/event\/[eventid]\/feedback"}]},{"label":"Settings","icon":"gear","child":[{"label":"General","url":"\/backpanel\/event\/[eventid]\/settings\/general"},{"label":"Forms","url":"\/backpanel\/event\/[eventid]\/settings\/forms"},{"label":"Email Template","url":"\/backpanel\/event\/[eventid]\/settings\/email-template"},{"label":"Notification","url":"\/backpanel\/event\/[eventid]\/settings\/notification"}]}]';
		// print serialize(json_decode($tevent, true));
		// print json_encode(unserialize($res));
		// $this->_generate_qrcode('SDD2017: ds8sd5');
		// var_dump($this->_generate_code_ticket());
		$this->_generate_ticket('GH4F2B');
		// $this->load->view('ticket/ticket');
		// var_dump($this->attendeemodel->generate_ticket('gjksk78sd'));

	}
}
